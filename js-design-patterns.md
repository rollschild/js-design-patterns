# JavaScript Design Patterns

## The Constructor Pattern

```javascript
// set properties
Object.defineProperty(newObj, "someKey", {
  value: "someValue",
  writable: true,
  enumberable: true,
  configurable: true,
});
```

- `Object.defineProperties()`
- Constructors with Prototypes
  - functions in JS have a `prototype` property

```javascript
function Car(model, year, miles) {
  this.model = model;
  this.year = year;
  this.miles = miles;
}

Car.prototype.toString = function () {
  return this.model + " has done " + this.miles + " miles";
};
```

## The Module Pattern

- Several options for implementing modules:
  - Object literal notation
  - The Module pattern
  - AMD modules
  - CommonJS modules
  - ECMAScript Harmony modules
- The Module Pattern is based on object literals
- **Privacy**
  - the module pattern encapsulates "privacy" state and organization using
    closures
- In the form similar to immediately-invoked function expression (IIFE)
- Pros:
  - supports private data
- Cons:
  - if we wanted to change visibility, we have to make changes to each place
    the member was used
  - cannot access private members in methods that are added to the object at
    a later point
  - inability to create automated tests for private members
  - not possible to patch privates

## The Singleton Pattern

- Restricts instantiation of a class to a single object
- In JS, singletons serve as a shared resource namespace which isolate
  implementation code from the global namespace so as to provide a single
  point of access for functions
- the sole instance should be extensible by subclassing, and clients should be
  able to use an extended instance without modifying their code
- The difference between a static instance of a class (object) and a Singleton:
  - while a Singleton can be implemented as a static instance, it can also be
    constructed _lazily_, without the use of resources or memory, until the
    static instance is needed
- The presence of the Singleton is often an indication that modules in a system
  are either tightly coupled or that logic is overly spread across multiple
  parts of a codebase

## Observer Pattern

- An object (a.k.a. a **subject**) maintains a list of objects (**observer**s) depending on it, automatically notifying them of any changes to state

### Publish/Subscribe Pattern

- Uses a topic/event channel that sits between the objects wishing to receive
  notifications (subscribers) and the object firing the event (the publisher)
- A good example: [PubSubJS](https://github.com/mroderick/PubSubJS/blob/master/src/pubsub.js)

## Mediator Pattern

- A mediator is a behavioral design pattern that allows us to expose a unified
  interface through which the different parts of a system may communicate
- Promotes loose coupling
- the Mediator pattern is essentially a shared subject in the Observer pattern
