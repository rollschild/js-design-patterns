const mySingleton = (function () {
  let instance;

  function init() {
    // Singleton

    function privateMethod() {
      console.log("I'm private");
    }

    const myPrivateVariable = "I'm also private";
    const privateRandomNumber = Math.random();

    return {
      publicMethod: function () {
        console.log("From publicMethod");
      },
      publicProperty: "I'm publicProperty!",
      getRandomNumber: function () {
        return privateRandomNumber;
      },
    };
  }

  return {
    getInstance: function () {
      if (!instance) {
        instance = init();
      }
      return instance;
    },
  };
})();

const singletonA = mySingleton.getInstance();
// console.log(singletonA.getRandomNumber());

const SingletonTester = (function () {
  function Singleton(options = {}) {
    this.name = "SingletonTester";
    this.pointX = options.pointX || 0;
    this.pointY = options.pointY || 0;
  }

  let instance;
  const _static = {
    name: "SingletonTester",
    getInstance: function (options) {
      if (!instance) {
        instance = new Singleton(options);
      }
      return instance;
    },
  };

  return _static;
})();
const singletonTest = SingletonTester.getInstance({ pointX: 123 });
// console.log(`pointX: ${singletonTest.pointX}, pointY: ${singletonTest.pointY}`);
