/*
 * https://github.com/ajacksified/Mediator.js/blob/master/lib/mediator.js
 */
(function (global, factory) {
  "use strict";

  if (typeof define === "function" && define.amd) {
    // AMD pattern
    define([], function () {
      global.Mediator = factory();
      return global.Mediator;
    });
  } else if (typeof exports !== undefined) {
    // Node/CommonJS
    exports.Mediator = factory();
  } else {
    // Browser global
    global.Mediator = factory();
  }
})(this, function () {
  "use strict";

  // a function that generates GUIDs for instances of the Mediator Subscribers
  // so we can easily reference them later on
  function guidGenerator() {
    const S4 = function () {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (
      S4() +
      S4() +
      "-" +
      S4() +
      "-" +
      S4() +
      "-" +
      S4() +
      "-" +
      S4() +
      S4() +
      S4()
    );
  }

  // subscriber constructor
  // subscribers are instances of Mediator channel registrations
  // an object instance is generated so that it can be later updated without being registered/unregistered
  function Subscriber(fn, options, context) {
    if (!(this instanceof Subscriber)) {
      return new Subscriber(fn, options, context);
    } else {
      this.id = guidGenerator();
      this.fn = fn;
      this.options = options;
      this.context = context;

      // similar to topic
      this.channel = undefined;
    }
  }

  Subscriber.prototype = {
    /*
     * takes an options object
     */
    update: function (options) {
      if (options) {
        this.fn = options.fn || this.fn;
        this.context = options.context || this.context;
        this.options = options.options || this.options;

        if (
          this.channel &&
          this.options &&
          this.options.priority !== undefined
        ) {
          this.channel.setPriority(this.id, this.options.priority);
        }
      }
    },
  };

  /*
   * a Mediator channel holds a list of sub-channels and subscribers to be fired
   * when Mediator.publish is called on the Mediator instance
   */
  function Channel(namespace, parent) {
    if (!(this instanceof Channel)) {
      return new Channel(namespace);
    }

    this.namespace = namespace || "";
    this._subscribers = [];
    this._channels = {};
    this._parent = parent;
    this.stopped = false;
  }

  /*
   * the Channel instance is passed as an argument to the Mediator subscriber
   */

  Channel.prototype = {
    sortChannelPriority: function (a, b) {
      if (a.options.priority > b.options.priority) {
        return -1;
      } else if (a.options.priority < b.options.priority) {
        return 1;
      } else {
        return 0;
      }
    },
    addSubscriber: function (fn, options, context) {
      if (!options || typeof options !== "object") {
        options = { priority: 0 };
      }

      options.priority = options.priority | 0;

      const subscriber = new Subscriber(fn, options, context);
      subscriber.channel = this;
      this._subscribers.push(subscriber);
      this._subscribers.sort(this.sortChannelPriority);

      return subscriber;
    },
    stopPropagation: function () {
      this.stopped = true;
    },
    getSubscriber: function (id) {
      return this._subscribers.find(sub => sub.id === id || sub.fn === id);
    },
    setPriority: function (id, priority) {
      const index = this._subscribers.findIndex(
        sub => sub.id === id || sub.fn === id,
      );
      if (index > -1) {
        this._subscribers[index].priority = priority;
      }
      this._subscribers.sort(this.sortChannelPriority);
    },
    addChannel: function (channel) {
      if (this._channels[channel]) {
        return;
      }
      this._channels[channel] = new Channel(
        (this.namespace ? this.namespace + ":" : "") + channel,
        this,
      );
    },
    hasChannel: function (channel) {
      return this._channels.hasOwnProperty(channel);
    },
    returnChannel: function (channel) {
      return this._channels[channel];
    },
    removeSubscriber: function (id, autoCleanup) {
      let p = this._subscribers.length - 1;

      autoCleanup =
        autoCleanup === null || autoCleanup === undefined ? true : autoCleanup;

      if (!id) {
        // if we don't pass an id, clean them all
        this._subscribers = [];
      } else {
        while (p >= 0) {
          if (
            this._subscribers[p].id === id ||
            this._subscribers[p].fn === id
          ) {
            this._subscribers[p].channel = undefined;
            this._subscribers.splice(p, 1);
          }
          p -= 1;
        }
      }

      if (autoCleanup) {
        this.autoCleanChannel();
      }
    },
    autoCleanChannel: function () {
      if (this._subscribers.length !== 0) {
        return;
      }

      // make sure no more sub-channels that have this channel has parent
      for (const key in this._channels) {
        if (this._channels.hasOwnProperty(key)) {
          return;
        }
      }

      // see if this channel has a parent
      // if it does NOT, do not remove it
      if (this._parent) {
        const path = this.namespace.split(":");
        const channelName = path[path.length - 1];
        this._parent.removeChannel(channelName);
      }
    },
    removeChannel: function (channel) {
      if (this.hasChannel(channel)) {
        this._channels[channel] = undefined; // or null?
        delete this._channels[channel];
      }
      this.autoCleanChannel();
    },
    publish: function (data) {
      let p = 0;
      let len = this._subscribers.length;
      let shouldCall = false;
      let numOfSubsBefore;

      while (p < len) {
        shouldCall = false;
        const subscriber = this._subscribers[p];

        if (!this.stopped) {
          numOfSubsBefore = this._subscribers.length;

          if (
            subscriber.options !== undefined &&
            typeof subscriber.options.predicate === "function"
          ) {
            if (subscriber.options.predicate.apply(subscriber.context, data)) {
              shouldCall = true;
            }
          } else {
            shouldCall = true;
          }
        }

        if (shouldCall) {
          // call the callback
          if (subscriber.options && subscriber.options.calls !== undefined) {
            subscriber.options.calls -= 1;
            if (subscriber.options.calls < 1) {
              this.removeSubscriber(subscriber.id);
            }
          }

          subscriber.fn.apply(subscriber.context, data);

          len = this._subscribers.length;
          if (numOfSubsBefore === this._subscribers.length) {
            p += 1;
          }
        }
      }

      if (this._parent) {
        this._parent.publish(data);
      }

      this.stopped = false;
    },
  };

  /*
   * A Mediator instance is the interface through which events are registered and removed from publish channels
   */
  function Mediator() {
    if (!(this instanceof Mediator)) {
      return new Mediator();
    }

    this._channels = new Channel("");
  }

  Mediator.prototype = {
    /*
     * if readOnly, do not create non-existing channels
     */
    getChannel: function (namespace, readOnly) {
      let channel = this._channels;
      if (namespace === "") {
        return channel;
      }
      const hierarchy = namespace.split(":");
      if (hierarchy.length > 0) {
        for (let i = 0; i < hierarchy.length; i += 1) {
          if (!channel.hasChannel(hierarchy[i])) {
            if (readOnly) {
              break;
            } else {
              channel.addChannel(hierarchy[i]);
            }
          }

          channel = channel.returnChannel(hierarchy[i]);
        }
      }

      return channel;
    },
    subscribe: function (channelName, fn, options, context) {
      const channel = this.getChannel(channelName || "", false);
      options = options || {};
      context = context || {};
      return channel.addSubscriber(fn, options, context);
    },
    once: function (channelName, fn, options, context) {
      options = options || {};
      options.calls = 1;
      return this.subscribe(channelName, fn, options, context);
    },
    getSubscriber: function (id, channelName) {
      const channel = this.getChannel(channelName || "", true);
      if (channel.namespace !== channelName) {
        return undefined;
      }
      return channel.getSubscriber(id);
    },
    remove: function (channelName, id, autoClean) {
      const channel = this.getChannel(channelName || "", false);
      if (channel.namespace !== channelName) {
        return false;
      }
      channel.removeSubscriber(id, autoClean);
    },
    publish: function (channelName, ...args) {
      const channel = this.getChannel(channelName || "", false);
      if (channel.namespace !== channelName) {
        return undefined;
      }

      args.push(channel); // I do not understand why

      channel.publish(args);
    },
  };

  Mediator.Channel = Channel;
  Mediator.Subscriber = Subscriber;
  return Mediator;
});
