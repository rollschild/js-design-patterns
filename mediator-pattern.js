const mediator = (function () {
  const topics = {};

  const subscribe = function (topic, fn) {
    if (!topics[topic]) {
      topics[topic] = [];
    }
    topics[topic].push({
      context: this,
      callback: fn,
    });
    return this;
  };

  const publish = function (topic, ...args) {
    if (!topics[topic]) {
      return false;
    }

    for (let i = 0, l = topics[topic].length; i < l; i += 1) {
      const subscription = topics[topic][i];

      // notice that args is an array
      subscription.callback.apply(subscription.context, args);
    }

    return this;
  };

  return {
    publish,
    subscribe,
    installTo: function (obj) {
      obj.subscribe = subscribe;
      obj.publish = publish;
    },
  };
})();

mediator.subscribe("message", console.log);
mediator.publish(
  "message",
  { message: "new message", recepient: "jovi" },
  "This is the email body",
);
