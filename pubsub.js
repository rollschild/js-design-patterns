let pubsub = {};

(function (q = {}) {
  const topics = {};
  let subUid = -1;

  q.publish = function (topic, args) {
    if (!topics[topic]) {
      return;
    }

    const subscribers = topics[topic];
    subscribers.forEach(({ func }) => {
      func(topic, args);
    });
  };

  q.subscribe = function (topic, func) {
    if (!topics[topic]) {
      topics[topic] = [];
    }
    subUid += 1;
    const token = String(subUid);
    topics[topic].push({
      func,
      token,
    });

    return token;
  };

  q.unsubscribe = function (token) {
    for (const [topic, array] of Object.entries(topics)) {
      for (let i = 0; i < array.length; i += 1) {
        const sub = array[i];
        if (sub.token === token) {
          topics[topic].splice(i, 1);
          return;
        }
      }

      // if (let index = array.findIndex((sub,) => sub.token === token) > -1) {
      //   topics[topic].splice(index, 1);
      // }
    }
  };
})(pubsub);

const messageLogger = function (topics, data) {
  const message = Array.isArray(data)
    ? data.join(", ")
    : typeof data === "object"
    ? JSON.stringify(data)
    : data;
  console.log("Logging: " + topics + ": " + message);
};
const subscription = pubsub.subscribe("inbox/newMessage", messageLogger);

pubsub.publish("inbox/newMessage", {
  sender: "jeff@amazon.com",
  body: "Good job!",
});

pubsub.unsubscribe(subscription);

pubsub.publish("inbox/newMessage", "Knock knock...");
