# "Pattern"-ity Testing, Proto-Patterns, and the Rule of Three

- Christopher Alexander:
  > A pattern should be both a process and a "thing".
  > The process should create the "thing".
