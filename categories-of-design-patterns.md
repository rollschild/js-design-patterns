# Categories of Design Patterns

- Creational Design Patterns
  - constructor
  - factory
  - abstract
  - prototype
  - singleton
  - builder
- Structural Design Patterns
  - decorator
  - facade
  - flyweight
  - adapter
  - proxy
- Behavioral Design Patterns
  - iterator
  - mediator
  - observer
  - visitor
