# Observable

- An observable is just a function, taking in an **observer** as an argument,
  and returning a **subscription object**
- A **subscription object** represents a disposable resource, such as the
  execution of an Observable
- The **observer** is an object, with three functions:
  - `next()`
  - `error()`
  - `complete()`
  - the observable will call these functions above ^
