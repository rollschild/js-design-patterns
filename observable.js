/*
 * https://medium.com/@fknussel/a-simple-observable-implementation-c9c809c89c69
 * https://betterprogramming.pub/an-introduction-to-the-observable-pattern-in-javascript-3e6ae0ebc22
 * https://netbasal.com/javascript-observables-under-the-hood-2423f760584
 * https://www.stackchief.com/tutorials/JavaScript%20Observables%20in%205%20Minutes
 */

const Observer = (onNext, onError = () => {}, onComplete = () => {}) => {
  let isUnsubbed = false;

  if (typeof onNext === "object") {
    onError = onNext.error || onError;
    onComplete = onNext.complete || onComplete;
    onNext = onNext.next || (() => {});
  }

  const next = (value) => {
    if (onNext && !isUnsubbed) {
      onNext(value);
    }
  };

  const unsubscribe = () => {
    isUnsubbed = true;
  };

  const error = (err) => {
    if (!isUnsubbed) {
      if (onError) {
        onError(err);
      }
    }
    unsubscribe();
  };

  const complete = () => {
    if (!isUnsubbed) {
      if (onComplete) {
        onComplete();
      }
      unsubscribe();
    }
  };

  return {
    next,
    error,
    complete,
    unsubscribe,
  };
};

// subscriber is a function that takes an observer
const Observable = (subscriber) => {
  // the arguments are essentially the observer
  const subscribe = (next, error, complete) => {
    const observer = Observer(next, error, complete);
    const unsubscribe = subscriber(observer);
    // returns a subscription object, which includes methods, including
    // `unsubscribe`
    return {
      unsubscribe: () => {
        if (typeof unsubscribe === "function") {
          unsubscribe();
        }
      },
    };
  };

  const fromEvent = (element, event) => {
    return Observable((observer) => {
      const handler = (e) => observer.next(e);
      element.addEventListener(event, handler);

      return () => {
        element.removeEventListener(event, handler);
      };
    });
  };

  const fromArray = (array) => {
    return Observable((observer) => {
      array.forEach((val) => observer.next(val));
      observer.complete();
    });
  };

  return {
    subscribe,
    fromEvent,
    fromArray,
  };
};

function interval(mils) {
  return Observable((subscriber) => {
    const timer = setInterval(subscriber.next, mils);
    return () => clearInterval(timer);
  });
}
const sub = interval(1000).subscribe({
  next: () => console.log("tick"),
});
setTimeout(() => sub.unsubscribe(), 5000);

let array = Observable().fromArray([2, 3, 5, 10]);
array.subscribe({ next: (val) => console.log("value: ", val) });
