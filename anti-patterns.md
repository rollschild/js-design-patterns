# Anti Patterns

- A bad design that is worthy of documenting
- Examples of anti-patterns in JS:
  - Polluting the global namespace by defining a large number of variables in
    the global context
  - Passing strings rather than functions to either `setTimeout` or
    `setInterval`, as this triggers the use of `eval()` internally
  - Modify the `Object` class prototype - particularly bad!
  - Use JS in an inline form as this is inflexible
  - The use of `document.write` where native DOM alternatives like
    `document.createElement` are more appropriate
    - if `document.write` is executed after the page has been load, it can
      actually overwrite the page we are on
    - it does not work on XHTML either
